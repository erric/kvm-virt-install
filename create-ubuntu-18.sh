virt-install -n iron \
        -r 1024 \
        --os-type=ubuntu17.10 \
        --vcpus=2 \
        --disk /var/lib/machines/linux/iron.img,device=disk,bus=virtio,size=60,sparse=true,format=raw \
        --network bridge=br0,model=virtio \
        --vnc \
        --vncport=5919 \
        --noautoconsole \
        --cdrom /var/lib/machines/ISO/ubuntu-18.04-live-server-amd64.iso