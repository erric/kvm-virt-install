virt-install -n opnsense \
-r 4096 \
--os-type=openbsd5.8 \
--vcpus=2 \
--disk /var/lib/machines/opnsense.img,device=disk,bus=virtio,size=60,sparse=true,format=raw \
--network bridge=br0.10,model=virtio \
--network bridge=br0,model=virtio \
--network bridge=br0.40,model=virtio \
--network bridge=br0.50,model=virtio \
--network bridge=br0.60,model=virtio \
--network bridge=br0.70,model=virtio \
--network bridge=br1.30,model=virtio \
--network bridge=br1.80,model=virtio \
--network bridge=br1.90,model=virtio \
--network bridge=br1.100,model=virtio \
--network bridge=br1.110,model=virtio \
--vnc \
--vncport=5923 \
--noautoconsole \
--cdrom /var/lib/libvirt/ISO/OPNsense-18.1-OpenSSL-dvd-amd64.iso